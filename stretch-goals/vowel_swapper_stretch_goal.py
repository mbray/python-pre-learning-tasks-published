def vowel_swapper(string):
    # ==============
    #   a becomes 4
    string = replace(string, "a", "A", "4")

    #   e becomes 3
    string = replace(string, "e", "E", "3")

    #   i becomes !
    string = replace(string, "i", "I", "!")

    #   o becomes ooo
    #   O becomes 000
    string = replace_multi(string, "o", "O", "ooo", "000")

    #   u becomes |_|
    string = replace(string, "u", "U", "|_|")

    #   return string output
    return string


def replace(string, lower, upper, new):
    occurrence = 0
    for i in range(len(string)):
        if string[i] in [lower, upper]:
            occurrence += 1
            if occurrence == 2:
                string = string[:i] + new + string[i + 1:]
    return string


# replace multiple characters
def replace_multi(string, lower, upper, new_lower, new_upper):
    occurrence = 0
    for i in range(len(string)):
        if string[i] in [lower, upper]:
            occurrence += 1
            if occurrence == 2:
                if string[i] == lower:
                    string = string[:i] + new_lower + string[i + 1:]
                else:
                    string = string[:i] + new_upper + string[i + 1:]
    return string

    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
