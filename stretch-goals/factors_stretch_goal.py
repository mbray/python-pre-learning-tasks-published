def factors(number):
    # ==============
    factors_array = []
    for num in range(2, number):
        if number % num == 0:
            factors_array.append(num)
    if not factors_array:
        return "{} is a prime number".format(number)
    else:
        return factors_array
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
