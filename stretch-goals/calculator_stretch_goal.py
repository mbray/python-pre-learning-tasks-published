def calculator(a, b, operator):
    # ==============
    result = None
    # addition
    if operator == "+":
        result = a + b
    # subtraction
    elif operator == "-":
        result = a - b
    # multiplication
    elif operator == "*":
        result = a * b
    # division
    elif operator == "/":
        result = a // b
    # if operator is unknown, return none
    return binary(result)


def binary(n):
    remainders = []
    # repeat until number reaches 0
    while n > 0:
        # add the remainder of n divided by 2 to the start of the list (as a string value)
        remainders.insert(0, str(n % 2))
        # divide number by 2
        n = n // 2
    # return remainders list as joined string
    return "".join(remainders)
    # ==============


print(calculator(2, 4, "+"))  # Should print 110 to the console
print(calculator(10, 3, "-"))  # Should print 111 to the console
print(calculator(4, 7, "*"))  # Should output 11100 to the console
print(calculator(100, 2, "/"))  # Should print 110010 to the console
