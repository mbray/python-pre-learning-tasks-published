def vowel_swapper(string):
    # ==============
    #   a becomes 4
    for c in ["a", "A"]:
        string = string.replace(c, "4")

    #   e becomes 3
    for c in ["e", "E"]:
        string = string.replace(c, "3")

    #   i becomes !
    for c in ["i", "I"]:
        string = string.replace(c, "!")

    #   o becomes ooo
    string = string.replace("o", "ooo")

    #   O becomes 000
    string = string.replace("O", "000")

    #   u becomes |_|
    for c in ["u", "U"]:
        string = string.replace(c, "|_|")

    #   return string output
    return string
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console